# flywayDB

## Why?

I wanted to play with [Flyway](https://documentation.red-gate.com/flyway)

## Docs?

Look
[here](https://documentation.red-gate.com/flyway/flyway-cli-and-api/concepts/migrations).

Simplified reasoning:

```
Letters have meaning:
B - BaseLine
V - Versioning
U - Undo
R - Repeatable (independent *)
```

## How?

```
# (with) docker-compose
docker-compose up flyway
```
