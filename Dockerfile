FROM gradle:8.5-jdk17

WORKDIR /app

RUN apt update && apt install -y netcat

VOLUME [ "/home/gradle/.gradle/caches" ]

COPY . .

CMD [ "gradle", "flywayMigrate", "flywayInfo" ]
